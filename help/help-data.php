<?php
# Help Panel - Data form
# V5
?>

	<h1>The Simple:Press V6 Data Importer</h1>

	<h3>Source database access</h3>

	<p>The Simple:Press Data Importer requires the necessary credentials along with any other pertinent informartion, to access the source
	forums's database. Please complete this form and note that fields marked with the red bullet are required to be filled.</p>

	<h3>If the source forum is a WordPress plugin</h3>
	<p>If your source forum is another WordPress plugin then the importer will require both the table prefix used for the forum source tables
	as well as the table prefix used for the WordPress 'Users' table - even if these are the same and the current source tables reside in the
	same database as the target Simple:Press tables.</p>
	
	<h3>Database Name, Database User Name and Database Password</h3>
	<p>If your source data is on the same database server and using the same database as this importer then you can find your database name in your <i>wp-config.php</i> file.  For example, if you are importing from another plugin that is installed on this site your database information will be in your WordPress <i>wp-config.php</i> file.</p>
	<p>Otherwise, if your database is located on a different server you will need to contact your admin to get this information.</p>	
	
	<h3>Database Host</h3>
	<p>If your source data is on the same database server as this importer AND the database engine (usuallly MYSQL or MARIADB) is also running on this server, then you can use <i>localhost</i> for the Database Host.</p>
	<p>If, for example, you are importing from another plugin that was previously installed on this WordPress site and the MYSQL server is being hosted on this same server you can use <i>localhost</i>. </p>
	<p>If your source data is on this same WordPress install you can also get your Database Host information from your <i>wp-config.php</i> file</p>

	<h3>Password handling</h3>
	<p>If a user being imported is found to already exist in the target database (by their login name) neither their password or their capabilities
	data will be changed by the importer. If not their capability will be set to 'subscriber'. Passwords imported from non-WordPress databases can
	be copied into the new user record but they will not work and will require the user to call for a password reset.</p>

	<h3>Encode results to utf-8</h3>
	<p>This option is usually not required but following the import if characters do not display correctly or posts are truncated then try again with this option turned on</p>