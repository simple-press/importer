<?php
/*
Plugin Name: Simple:Press V6 Importer and Import Framework
Plugin URI: https://simple-press.com/downloads/simple-press-importer-plugin/
Description: Imports data into Simple:Press from bbPress and other legacy forums.  Includes a framework for developers to quickly and easily build new Data Importers. A new importer can be built by an experienced developer in as little as 2-3 days.
Version: 3.2.2
Author: Simple:Press
Original Author: Andy Staines & Steve Klasen
Author URI: http://simple-press.com
WordPress Version 4.9 and above
Simple-Press Version 6.1.0 and above
*/

if (!class_exists('spcSimplePress')) {
    function simple_press_import_admin_notice__error() {
        $class = 'notice notice-error';
        $message = __( 'To be able to use Simple:Press Importer you have to have Simple:Press activated' );

        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
    }
    add_action( 'admin_notices', 'simple_press_import_admin_notice__error' );
    return;
}

/*  Copyright 2012  Simple:Press, Andy Staines & Steve Klasen

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For a copy of the GNU General Public License, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

# ------------------------------------------------------------------
# Define Constants
# ------------------------------------------------------------------
	define('SPI_URL',			WP_PLUGIN_URL.'/sp-import/');	# Plugin folder url
	define('SPI_DIR',			WP_PLUGIN_DIR.'/sp-import/');	# Plugin path
	
	// Define some constants that this plugin expects.  Since this plugin was initially 
	// written prior to SP V 6.x it uses some old constants.  So we're just going to assign
	// the new V6 constants to those old constants because those old constants no longer 
	// exist in V6.  So, there should be no conflict.
	define('SFMEMBERS',			SPMEMBERS);  
	define('SFGROUPS',			SPGROUPS);
	define('SFDEFPERMISSIONS',	SPDEFPERMISSIONS);
	define('SFPERMISSIONS',		SPPERMISSIONS);
	define('SFROLES',			SPROLES);
	//define('SFMEMBERS',			SPMEMBERS);
	define('SFMEMBERSHIPS',		SPMEMBERSHIPS);
	define('SFFORUMS', 			SPFORUMS);
	define('SFTOPICS',			SPTOPICS);
	define('SFPOSTS',			SPPOSTS);
	
	

# ------------------------------------------------------------------
# Action/Filter Hooks
# Create new menu item in the WP Simple:Press menu
# ------------------------------------------------------------------
add_action('admin_menu', 				'spi_menu');

# ------------------------------------------------------------------------------------------
# Determine quickly if admin and then if importer admin page load

if(is_admin() && isset($_GET['page']) && stristr($_GET['page'], 'sp-import/')) {
	add_action('admin_print_styles', 	'spi_css');
	add_action('admin_enqueue_scripts', 'spi_scripts');
}

# ------------------------------------------------------------------
# spi_menu()
# Add the importer to SP menu item into th SP menu
# ------------------------------------------------------------------
function spi_menu() {
	$url = 'sp-import/admin/spimport-setup.php';
	add_submenu_page('simplepress/admin/panel-forums/spa-forums.php', esc_attr('Importer'), esc_attr('Importer For SP 6.x'), 'read', $url);
}

# ------------------------------------------------------------------
# spa_load_admin_css()
# Loads up the forum admin CSS
# ------------------------------------------------------------------
function spi_css() {
	wp_register_style('spImportStyle', SPI_URL.'css/spimport.css');
	wp_enqueue_style( 'spImportStyle');
}

# ------------------------------------------------------------------
# spi_scripts()
# Load up the javascript we need
# ------------------------------------------------------------------
function spi_scripts() {
	wp_register_script('jquery', WPINC.'/js/jquery/jquery.js', false, false, false);
	wp_enqueue_script('jquery');
	wp_enqueue_script('spi', SPI_URL.'jscript/spimport.js', array('jquery'), false);
}

?>