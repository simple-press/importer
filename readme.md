# README #

This plugin is designed to import data into the Simple:Press forum plugin.

### How do I get set up? ###

Installation is straightforward using the usual WordPress Plugins -> Add New procedure.

- Download the importer .zip file to your desktop.  You can get it for free from our store.
- Within WordPress Dashboard, click `Plugins` -> `Add New`
- Click the `Upload` button and select the ZIP file you just downloaded.
- Click the `Install` button
- If all goes well you can click the `Activate` link.


### Change Log  ###

3.2.2
-----
Fix: A bad call to the add-user function...

3.2.1
-----
Fix: A form had an incorrect name in the WPForo importer.  No real-world issues, just a naming convention issue.
Fix: The "working" spinner was not being shown because it was referencing the simple:press folders using the 5.0 naming convention instead of the 6.0 names.

3.2.0
-----
Enh: Added importer for ASGAROS forums
Enh: Added importer for WPForo forums
Fix: Better handling for topics and posts with embedded single and double quotes
Add: Simple:Press offers and link to contact form on all pages
Enh: Additional help text.

3.1.0
-----
Enh: Updated to import data into Simple:Press 6.x